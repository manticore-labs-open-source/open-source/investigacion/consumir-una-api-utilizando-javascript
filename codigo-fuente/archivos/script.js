const app = document.getElementById('root');

const logo = document.createElement('img');
logo.src = 'amiibo-logo.png';

const container = document.createElement('div');
container.setAttribute('class', 'container');

app.appendChild(logo);
app.appendChild(container);

var request = new XMLHttpRequest();
request.open('GET', 'http://www.amiiboapi.com/api/amiibo', true);
request.onload = function () {

var datos = JSON.parse(this.response);
  if (request.status >= 200 && request.status < 400) {
    datos.amiibo.forEach(figura => {
      const card = document.createElement('div');
      card.setAttribute('class', 'card');

      const titulo = document.createElement('h1');
      titulo.textContent = figura.character;

      const imagen = document.createElement('img');
      imagen.setAttribute('src', `${figura.image}`);

      const contenido = document.createElement('p');
      figura.gameSeries = figura.gameSeries.substring(0, 1000);
      contenido.textContent = `Serie de juegos: ${figura.gameSeries}`;

      container.appendChild(card);
      card.appendChild(titulo);
      card.appendChild(imagen);
      card.appendChild(contenido);
    });
  } 

  else {
    const errorMessage = document.createElement('marquee');
    errorMessage.textContent = `Esto no está funcionando`;
    app.appendChild(errorMessage);
  }
}

request.send();