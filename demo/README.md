# Aclaración

Como tal, no existe un proyecto compilado de la investigación, para probar el proyecto, se deben seguir los siguientes pasos:

1. Clonar el presente repositorio
2. Ingresar en la ruta *codigo-fuente/archivos*
3. Dar doble clic en el archivo **index.js**
4. El contenido se mostrará directamente en el navegador predefinido