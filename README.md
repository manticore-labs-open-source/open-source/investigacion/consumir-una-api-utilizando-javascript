# Consumir una API utilizando JavaScript

## Prerequisitos

Para poder consumir una API utilizando JavaScript, se necesitan los siguientes conocimientos a manera de prerequisitos:

* Conocimientos básicos en JavaScript, HTML y CSS
* Nociones básicas de operaciones con arreglos en JavaScript (Dando clic [aquí](https://gitlab.com/manticore-labs/open-source/investigacion/arrays-javascript) podrá encontrar una pequeña guía a su utilización)
* Nociones básicas de operaciones con JSON (Dando clic [aquí](https://gitlab.com/manticore-labs/open-source/investigacion/operaciones-en-json) podrá encontrar una pequeña guía a su utilización)

## ¿Qué es una API?

Por sus siglas en inglés, se refiere a una *_Interfaz de Programa de Aplicación_* (Application Program Interface) que puede ser definida como el conjuto de métodos de comunicación entre varios componentes software. Para la presente investigación, se utilizará una API Web, la cual permite a un servidor web interactuar con software de terceros a través de métodos HTTP.

## Configuraciones

La API que será consumida será la API de *_amiibos_* de Nintendo, la cual puede ser accedada [aquí](https://gitlab.com/manticore-labs/open-source/investigacion/consumir-una-api-utilizando-javascript/tree/master/archivos) y se utilizará JavaScrip para mostrar, por ejemplo, el nombre de la figura, la serie de videojuego a la que pertenece y una imagen de la misma. Para conseguirlo, serán necesarios tres archivos principales:  **script.js**, **index.html** y **style.css** los cuales pueden ser encontrados [aquí](https://gitlab.com/manticore-labs/open-source/investigacion/consumir-una-api-utilizando-javascript/tree/master/archivos). El archivo **index.html** tiene la única función de contenedor, pues este archivo redirigirá a los archivos JavaScript y CSS en front.

## Conexión a la API

En primer lugar, es necesario determinar el *_endpoint_* de la API a utilizar para acceder directamente a los archivos JSON. Para este caso, el *_endpoint_* de la API será: [http://www.amiiboapi.com/api/amiibo/](http://www.amiiboapi.com/api/amiibo/) y como se muestra en la imagen a continuación, se observa una serie de JSON que es el contenido de la API como tal.

![JSON de la API](codigo-fuente/imagenes/json.png)

Ahora bien, conociendo ya el *_endpoint_* de la API, lo primero que hay que hacer, en el archivo JavaScript, es abrir una conexión a la API a través de uso de objetos **XMLHttpReques**. Por tanto, es necesario crear una variable para llamar a dicho objeto así:

``` javascript
var request = new XMLHttpRequest();
```

Con la variable asignada al objeto deseado, se puede crear una nueva conexión a través del método **open()** especificando el *_endpoint_* de la API y el método HTTP a utilizar, que en este caso será el método GET, entonces se tiene: 

``` javascript
request.open('GET', 'http://www.amiiboapi.com/api/amiibo/', true);

request.onload() = function() {
	
}
```

Se puede observar, se define una función una vez se haya abierto la conexión con la API, dentro de dicha función se realizará la manipulación de los objetos JSON y la creación de la vista para HTML.

## Trabajando con JSON

Como se menciona anteriormente, dentro de la función definida se creará la lógica para el manejo de JSON y para esto, se creará una nueva variable y se hará un **JSON.parse()** para su utilización. Para comprobar que efectivamente se realizó de manera correcta la conexión con la API, se realizará un *_forEach_* para listar los elementos, así:

``` javascript
var datos = JSON.parse(this.response);

datos.amiibo.forEach(figuras => {
	console.log(figuras);
});
```

**Nota:** Fue necesario utilizar el *_.amiibo_* a la variable creada antes de aplicar el *_forEach_* debio a la naturaleza de la API.

Si se abre el archivo **index.html** en un navegador, clic derecho en *_Inspeccionar_* y luego escoger la consola, se mostrará algo como lo siguiente:

![JSON de la API](codigo-fuente/imagenes/json-response.png)

Lo único restante es manejar los errores en caso de que la URL usada esté rota o no exista, para esto, se utilizarán los **códigos de estado HTTP**. Entonces, al código mostrado anteriormente, se lo colocará dentro de una sentencia *_if_* para que el contenido se muestre con códigos de estado entre 200 y 300 así:

``` javascript
var datos = JSON.parse(this.response);

  if (request.status >= 200 && request.status < 400) {
    datos.amiibo.forEach(figura => {
      console.log(figuras);
    });
  } 

  else {
    console.log('Error');
  }
```

Con todo esto, ya se está accediendo correctamente a la API, lo que resta, es la visualización de los datos y para esto, se utilizará el *_Modelo de Documento del Objeto_* (DOM) desde JavaScript.

## Visualización de los datos

Como ya se mención, se utilizará DOM para mostrar los datos en web; DOM por si mismo, es una API de JavaScript que permite la comunicación con HTML. Dentro del archivo **index.html** se definió un ID llamado *_root_* por tanto, para acceder a este archivo desde JavaScript se crea una constante así:

``` javascript
const app = document.getElementById('root');
```

Para mostrar el contenido dentro del **idex.html** será necesario crear una constante *_container_* en la cual se guarde el contenido deseado. Para esto, se realiza lo siguiente:

``` javascript
const container = document.createElement('div');
container.setAttribute('class', 'container');
```
**Nota:** Es importante agregar la nueva variable dentro del archivo **index.html** y para esto, se utiliza el método *_appendChild()_* así:

``` javascript
app.appendChild(container);
```

Ahora bien, dentro del *_forEach_* realizado anteriormente, se definiran tres elementos: un título, un espacio para la imagen y un texto que indique la clase de juego a la que pertenece la figura. Todos estos elementos, estarán encapsulados dentro de un *_card_* HTML y para esto, es necesario aplicar las siguientes líneas de código:

``` javascript
datos.amiibo.forEach(figura => {
	const card = document.createElement('div');
    card.setAttribute('class', 'card');

    const titulo = document.createElement('h1');
    titulo.textContent = figura.character;

    const imagen = document.createElement('img');
    imagen.setAttribute('src', `${figura.image}`);

    const contenido = document.createElement('p');
    figura.gameSeries = figura.gameSeries.substring(0, 1000);
    contenido.textContent = `Serie de juegos: ${figura.gameSeries}`;

    container.appendChild(card);
    card.appendChild(titulo);
    card.appendChild(imagen);
    card.appendChild(contenido);
});
```

Finalmente, con el llamado al archivo de estilos CSS desde el archivo **index.html** y con lo realizado dentro del archivo JavaScript, se obtendrá algo similar a lo que se muestra en la imagen:

![JSON de la API](codigo-fuente/imagenes/visualizacion-datos.png)

<a href="https://twitter.com/following" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @BAndresTorres </a><br>
<a href="https://www.facebook.com/bryan.a.torres.5" target="_blank"><img alt="Sígueme en Facebook" height="35" width="35" src="https://sguru.org/wp-content/uploads/2018/02/Facebook-PNG-Image-71244.png" title="Sígueme en Facebook"/> Andrés Torres Albuja </a><br>
<a href="https://www.instagram.com/adler.luft/" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> adler.luft </a><br>

